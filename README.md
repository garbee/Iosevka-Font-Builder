# Font builder

Steps to use:

1. Clone repo
1. Edit `custom-build.sh` so the configuration of the font is as you'd like
1. Run `custom-build.sh` as your normal user. The sudo password will be provided to the script internally.
1. Chown the output to your user
1. Move the output to your font location
