FROM ubuntu:16.04

COPY premake5 /usr/local/bin

RUN chmod +x /usr/local/bin/premake5 \
    && apt-get update -qq -y \
    && apt-get install -qq -y ttfautohint curl build-essential unzip woff-tools git
RUN git clone --depth=1 --recursive https://github.com/google/woff2.git woff2 \
    && cd woff2 \
    && make clean all \
    && mv -t /usr/local/bin woff2_compress  woff2_decompress  woff2_info \
    && cd .. \
    && rm -rf woff2 \
    && curl -sL https://deb.nodesource.com/setup_7.x | bash - \
    && apt-get install -qq -y nodejs \
    && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/caryll/otfcc.git otfcc --depth=1 \
    && cd otfcc \
    && premake5 gmake \
    && cd build/gmake \
    && make config=release_x64 \
    && cd / \
    && mv otfcc/bin/release-x64/* /usr/local/bin \
    && rm -rf otfcc
RUN git clone https://github.com/be5invis/Iosevka.git Iosevka --depth=1 \
    && cd Iosevka \
    && npm install
