#!/bin/bash

echo 'Root privileges are needed to run docker'
read -s -p 'Enter password for sudo: ' sudoPW
echo ''

IMAGESUM=$(echo $sudoPW | sudo -S docker build -q . | sed -e "s/^sha256://")
echo $sudoPW | sudo -S docker run \
    --entrypoint=/bin/bash \
    --rm \
    -i \
    -v $PWD/output:/build \
    "$IMAGESUM" <<COMMANDS
rm -rf /build/*
cd Iosevka
make custom-config set='custom' weights='thin extralight light book medium bold heavy' design='cv01 cv04 cv08 cv12 cv14 cv17 cv19 cv21 cv26 cv27 cv29 cv33 cv36 cv38 cv40 cv42 cv44 cv47 type'
make custom set='custom'
make custom-web set='custom'
make custom-config set='customterm' weights='thin extralight light book medium bold heavy' design='cv01 cv04 cv08 cv12 cv14 cv17 cv19 cv21 cv26 cv27 cv29 cv33 cv36 cv38 cv40 cv42 cv44 cv47 term'
make custom set='customterm'
make custom-web set='customterm'
mv dist/* /build
exit
COMMANDS
echo $sudoPW | sudo -S chown -R $USER:$USER output/
